//Projeto Formula - Sensor de Temperatura DS18B20 (parte2)
//Jose Rafael Fernandes Carneiro da Cunha

#include <OneWire.h>
#include <DallasTemperature.h>
#define ONE_WIRE_BUS 3
#define TEMPERATURE_PRECISION 12

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

DeviceAddress thermometerOne = {0x28, 0x07, 0x3F, 0x83, 0x03, 0x00, 0x00, 0xC5};
DeviceAddress thermometerTwo = {0x28, 0xFF, 0x9E, 0xA8, 0x64, 0x15, 0x02, 0x7C};

int pinRele = 4;
int pinLed = 5;

void setup() 
{
 Serial.begin(9600);
 sensors.begin();

 Serial.println("initialising...");
 Serial.println();

 sensors.setResolution(thermometerOne, TEMPERATURE_PRECISION);
 sensors.setResolution(thermometerTwo, TEMPERATURE_PRECISION);

 pinMode(pinRele, OUTPUT);
 pinMode(pinLed, OUTPUT);

 digitalWrite(pinRele, HIGH);
}

void printTemperature()
{
  float tempC1 = sensors.getTempC(thermometerOne);
  float tempC2 = sensors.getTempC(thermometerTwo);
  Serial.print(" temp C1: ");
  Serial.print(tempC1 );
  Serial.print(" temp C2: ");
  Serial.print(tempC2 );

  
  if((tempC1>=33.00) || (tempC2 >= 33.00))
  {
    digitalWrite(pinRele, LOW);
    digitalWrite(pinLed, HIGH);
  }
  if((tempC1 < 33.00) && (tempC2 < 33.00))
  {
    digitalWrite(pinRele, HIGH);
    digitalWrite(pinLed, LOW);
  }
}

void loop()
 
{
  sensors.requestTemperatures();
 //print das temperaturas
 Serial.print ("Temperature: ");
 printTemperature();
 Serial.println();
 delay (3000); 
}
 

//Projeto Formula-Sensor de Temperatura DS18B20 (parte1)
//José Rafael Fernandes Carneiro da Cunha

#include <OneWire.h>
#include <DallasTemperature.h>
#define ONE_WIRE_BUS 3

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
DeviceAddress thermometerOne, thermometerTwo;

void setup() 
{
 Serial.begin(9600);
 sensors.begin();

 Serial.print ("Locating devices...");
 Serial.print ("Found");
 Serial.print (sensors.getDeviceCount(), DEC);
 Serial.println ("devices.");

 if (!sensors.getAddress(thermometerOne, 0)) 
 Serial.println ("Unable to find address for Device 0");
 if (!sensors.getAddress(thermometerTwo,1))
 Serial.println ("unable to find address for Device 1");

//print the addresses of both devices
 Serial.print("Device 0 Address: ");
 printAddress(thermometerOne);
 Serial.println();

 Serial.print("Device 1 Address: ");
 printAddress(thermometerTwo);
 Serial.println();
 Serial.println();
}

//function to print a device address
void printAddress (DeviceAddress deviceAddress)
{
  for(int i = 0; i<8; i++)
  {
   if(deviceAddress[i]<16) 
   Serial.print("0");
   Serial.print(deviceAddress[i], HEX);
  }
}

//function to print the temperature for a device
void printTemperature (DeviceAddress deviceAddress)
{
  float tempC = sensors.getTempC(deviceAddress);
  Serial.print ("Temp C: ");
  Serial.print (tempC);
}

//main function to print information about a device
void printData(DeviceAddress deviceAddress)
{
  Serial.print ("Device Address: ");
  printAddress(deviceAddress);
  Serial.print(" ");
  printTemperature(deviceAddress);
  Serial.println();
}

void loop() 
{
 //call sensors.requestTemperatures() to issue a global temperature
 //request to all devices on the bus
 Serial.print ("Requesting temperatures...");
 sensors.requestTemperatures();
 Serial.println ("DONE");

 //print the device information
 printData(thermometerOne);
 printData(thermometerTwo);
 Serial.println();
 delay(1000);
}

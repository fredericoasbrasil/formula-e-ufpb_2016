# Equipe E - UFPB

A equipe E-UFPB é participante da competição Formula SAE Brasil, categoria elétrico.

Neste espaço estará a solução desenvolvida para atender a regra 2015/2016 da competição no que diz respeito ao sensoriamento do carro, pedais de acelerador/freio e temperatura de baterias. Também serão desenvolvidas interfaces gráficas para exibição dos dados.

## Membros da equipe (2016):

 Abraão Allysson dos Santos Honório
 Fellipe André Lucena de Oliveira
 Filipe do Ó Cavalcanti
 Filype Anastácio Nascimento Silva
 Frederico Augusto dos Santos Brasil
 Heitor Augusto
 Helder Alan Inocencio da Silva
 Henrique Raldi Schlickmann
 Jose Rafael
 Josias Ferreira
 Lívia Regina Lima Alves
 Marcelo Geisler de Brito Lira
 Mikaelly Felicio Pedrosa
 Sara Mirthis Dantas dos Santos
 Ullisses Lopes Monteiro

